﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Gymnastlist;
using System.Collections.Generic;

namespace TestGymnastlist
{
    [TestClass]
    public class TestListManager
    {

        #region easyTest
        [TestMethod]
        public void AddItem_AddOneIntegerInNewList_Success()
        {
            //given
            ListManager listManager = new ListManager();
            int expectedInteger = 22;

            //when
            listManager.AddItem(expectedInteger);

            //then
            Assert.AreEqual(expectedInteger, listManager.Numbers[0]);
        }

        [TestMethod]
        public void AddItem_AddOneIntegerInExistingList_Success()
        {
            //given
            ListManager listManager = new ListManager();
            int integerToAdd = 5;
            List<double> expectedList = new List<double>();
            expectedList.Add(4);
            expectedList.Add(integerToAdd);

            listManager.AddItem(4);

            //when
            listManager.AddItem(integerToAdd);

            //then
            CollectionAssert.AreEqual(expectedList, listManager.Numbers);

        }
        #endregion easyTest

        #region mediumTest
        [TestMethod]
        public void Order_OrderListFromSmallestToBiggest_Success()
        {
            //given
            ListManager listManager = new ListManager();
            List<int> listToOrder = new List<int>() { 5, 4, 6 };
            List<int> expectedList = new List<int>() { 4, 5, 6 };
            listManager.AddItem(listToOrder);

            //when
            listManager.Order();

            //then
            Assert.AreEqual(expectedList.Count, listManager.Numbers.Count);
            Assert.AreEqual(expectedList[0], listManager.Numbers[0]);
            Assert.AreEqual(expectedList[1], listManager.Numbers[1]);
            Assert.AreEqual(expectedList[2], listManager.Numbers[2]);
        }

        [TestMethod]
        public void Order_OrderListFromBiggestToSmaller_Success()
        {
            //given
            ListManager listManager = new ListManager();
            List<int> listToOrder = new List<int>() { 5, 4, 6 };
            List<int> expectedList = new List<int>() { 6, 5, 4 };
            listManager.AddItem(listToOrder);

            //when
            listManager.Order(false);

            //then
            Assert.AreEqual(expectedList.Count, listManager.Numbers.Count);
            Assert.AreEqual(expectedList[0], listManager.Numbers[0]);
            Assert.AreEqual(expectedList[1], listManager.Numbers[1]);
            Assert.AreEqual(expectedList[2], listManager.Numbers[2]);
        }
        #endregion mediumTest

        #region hardTest
        [TestMethod]
        public void FilterByMultiple_MultipleBy3KeptsItems_Success()
        {
            //given
            ListManager listManager = new ListManager();
            List<int> listToFilter = new List<int>() { 5, 4, 6 };
            List<int> expectedList = new List<int>() { 5, 4 };
            listManager.AddItem(listToFilter);

            //when
            listManager.FilterByMultiple(3);

            //then
            Assert.AreEqual(expectedList.Count, listManager.Numbers.Count);
            Assert.AreEqual(expectedList[0], listManager.Numbers[0]);
            Assert.AreEqual(expectedList[1], listManager.Numbers[1]);
        }

        [TestMethod]
        public void FilterByMultiple_MultipleBy3RemovedItems_Success()
        {
            //given
            ListManager listManager = new ListManager();
            List<int> listToFilter = new List<int>() { 5, 4, 6 };
            List<int> expectedList = new List<int>() { 6 };
            listManager.AddItem(listToFilter);

            //when
            listManager.FilterByMultiple(3, false);

            //then
            Assert.AreEqual(expectedList, listManager.Numbers.Count);
            Assert.AreEqual(expectedList[0], listManager.Numbers[0]);
            #endregion hardTest
        }
    }
}